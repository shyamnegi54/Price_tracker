var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var product = new Schema({

  USERID: String,
  productName: String,
  CurrentPrice:Number,
  RequestedPrice: Number,
  isDeleted: { type: Boolean, default: false },
  islive: {
    type: Boolean, default: false
  }
});

module.exports = mongoose.model('Product', product);
