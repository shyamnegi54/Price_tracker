import axios from 'axios';

// async function getProductPriceData(asins, locale) {
//   const apiKey = '882814b36cmsh18c5ff8539882a6p1f9afejsn6ea6e50acda9';
//   const apiUrl = 'https://amazon-product-price-data.p.rapidapi.com/product';

//   const options = {
//     method: 'GET',
//     url: apiUrl,
//     params: {
//       asins: asins,
//       locale: locale
//     },
//     headers: {
//       'X-RapidAPI-Key': apiKey,
//       'X-RapidAPI-Host': 'amazon-product-price-data.p.rapidapi.com'
//     }
//   };

//   try {
//     const response = await axios.request(options);

//     if (response.status === 200) {
//       const data = response.data;
//       return data;
//     } else {
//       throw new Error('Request failed with status: ' + response.status);
//     }
//   } catch (error) {
//     console.error('Error fetching data:', error);
//     return null;
//   }
// }

// // Example usage
// const asins = 'B005YQZ1KE,B074R8RQQ2'; // Replace with your ASINs
// const locale = 'US'; // Replace with your desired locale

// getProductPriceData(asins, locale)
//   .then(data => {
//     if (data) {
//       console.log('Product price data:', data);
//     } else {
//       console.log('No data received.');
//     }
//   });