var express = require('express');
const Product = require('../models/product');
var Admin = require("../models/admin");
const jwt = require('jsonwebtoken');
const cors = require('cors');
require("dotenv").config();

const { getProductPriceData } = require('./details')
let {
  encryptPassword,
  comparePasswords,
  generateJwt,
} = require("../utils/loginutil.js");



var router = express.Router();

router.use(
  cors({

    credentials: true,
    origin: 'http://localhost:3000'

  })
)



router.post('/addproducts', async (req, res) => {
  try {
    let product = await new Product(req.body).save();
    res.json({
      message: "Product added",
      data: product, success: true
    })
  }
  catch (err) {
    res.json({ message: err.message, success: false })
  }
});

router.post('/productlist', async (req, res) => {
  try {
    const listproduct = await Product.find({ USERID: req.body.email });
    res.json({
      message: "Detailed product listing",
      data: listproduct,
      success: true
    })
  }
  catch (err) {
    res.json({ message: err.message, success: false })
  }
});


router.post('/updateproduct/:id', async (req, res) => {
  try {
    const productId = req.params.id;
    const updateData = req.body;

    const updatedProduct = await Product.findByIdAndUpdate(productId, updateData, {
      new: true,
    }).exec();

    if (!updatedProduct) {

      return res.status(404).json({ message: 'Product not found', success: false });
    }

    res.json({ message: 'Product successfully updated', product: updatedProduct, success: true });
  } catch (err) {

    res.status(500).json({ message: err.message, success: false });
  }
});

router.get('/products/:id', async (req, res) => {
  try {
    const productId = req.params.id;


    const product = await Product.findById(productId);

    if (!product) {

      return res.status(404).json({ message: 'Product not found', success: false });
    }


    res.json({ message: 'Product details fetched successfully', data: product, success: true });
  } catch (err) {

    res.status(500).json({ message: err.message, success: false });
  }
});


router.delete('/deleteproduct/:id', async (req, res) => {
  try {
    const productId = req.params.id;


    const deletedProduct = await Product.findByIdAndRemove(productId).exec();

    if (!deletedProduct) {
      return res.status(404).json({ message: 'Product not found', success: false });
    }

    res.json({ message: 'Product successfully deleted', success: true });
  } catch (err) {

    res.status(500).json({ message: err.message, success: false });
  }
});



router.post('/register', async (req, res) => {
  try {

    const AdminEmailCheck =
      await Admin.findOne(
        { email: new RegExp(`^${req.body.email}$`, 'i') }).exec();

    if (AdminEmailCheck)
      throw new Error('Email already registered');

    req.body.password = await encryptPassword(req.body.password);

    let admin = await new Admin(req.body).save();
    res.status(200).json({ message: "Admin Register Successfully", data: admin, success: true });


  }
  catch (err1) {
    console.error(err1);
    if (err1.message)
      res.json({ message: err1.message, data: err1, success: false });
    else
      res.json({ message: 'Error', data: err1, success: false });
  }
})



router.post('/login', async (req, res) => {
  try {

    const admin =
      await Admin.findOne
        ({
          email: new RegExp(`^${req.body.email}$`, 'i')
        }).exec();
    if (!admin)
      throw new Error("You are not registered");

    const checkPassword = await comparePasswords(req.body.password, admin.password);

    if (!checkPassword) {
      throw new Error("Check Your Credentials");
    }

    const token = jwt.sign({ email: admin.email, id: admin._id, name: admin.Name }, `${process.env.SECRET_KEY}`);
    return res
      .cookie("access_token", token, {
        httpOnly: true,
        secure: process.env.NODE_ENV === "production",

      })
      .status(200)
      .json({ message: "Logged in successfully 😊 👌", email: req.body.email });

  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, success: false });
    else
      res.json({ message: 'Error', success: false });
  }
})

const authorization = (req, res, next) => {
  const token = req.cookies.access_token;
  if (!token) {
    return res.sendStatus(403);
  }
  try {
    const data = jwt.verify(token, `${process.env.SECRET_KEY}`);
    req.userId = data.id;
    req.email = data.email;
    req.Name = data.name;
    return next();
  } catch {
    return res.sendStatus(403);
  }
};

router.get("/protected", authorization, (req, res) => {
  return res.json({ user: { id: req.userId, email: req.email, name: req.Name } });
});
router.get("/logout", authorization, (req, res) => {
  return res
    .clearCookie("access_token")
    .status(200)
    .json({ message: "Successfully logged out 😏 🍀" });
});
router.get("/profile", (req, res) => {
  const token = req.cookies.access_token;

  if (token) {
    jwt.verify(token, `${process.env.SECRET_KEY}`, {}, (err, user) => {
      if (err) {
        res.clearCookie("access_token");
        res.json(null);
      } else {
        res.json(user);
      }
    });
  } else {
    res.json(null);
  }
});

router.post("/fetchproduct", (req, res) => {
  if (req.body.url) {
    const { url } = req.body
    console.log(req.body)
    getProductPriceData(null, null, url)
      .then(data => {
        if (data) {
          // console.log('Product price data:', data);
          return res.json({ data: data });
        } else {
          console.log('No data received.');
          return res.json({ data: null });
        }
      });
  } else {
    const { asin, locale } = req.body;
    getProductPriceData(asin, locale)
      .then(data => {
        if (data) {
          // console.log('Product price data:', data);
          return res.json({ data: data });
        } else {
          console.log('No data received.');
          return res.json({ data: null });
        }
      });
  } 
  // console.log(req.body)
});
// router.post("/fetchproduct", (req, res) => {
//   if (req.body.url) {
//     const { url } = req.body;
//     console.log(req.body);
//     getProductPriceData(null, null, url)
//       .then(data => {
//         if (data && data.current_price !== undefined) {
//           const currentPrice = data.current_price;
//           return res.json({ current_price: currentPrice }); // Return only the current_price
//         } else {
//           console.log('No data received.');
//           return res.json({ current_price: null });
//         }
//       });
//   } else {
//     const { asin, locale } = req.body;
//     getProductPriceData(asin, locale)
//       .then(data => {
//         if (data && data.current_price !== undefined) {
//           const currentPrice = data.current_price;
//           return res.json({ current_price: currentPrice }); // Return only the current_price
//         } else {
//           console.log('No data received.');
//           return res.json({ current_price: null });
//         }
//       });
//   } 
// });
module.exports = router;


