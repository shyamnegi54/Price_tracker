const axios = require("axios")
function extractAsinAndCountryFromLink(link) {
  try {
    const asinRegex = /\/(dp|product)\/(\w{10})/;
    const countryRegex = /^https?:\/\/(www\.)?amazon\.([a-z\.]+)\//;
  
    const asinMatch = link.match(asinRegex);
    const countryMatch = link.match(countryRegex);
  
    if (asinMatch && countryMatch) {
      const asin = asinMatch[2];
      const locale = countryMatch[2];
      return asin
    } else {
      return null;
    }
  } catch{
    return null;
  }
};

async function getProductPriceData(asin, locale, url) {
    const asinValue = extractAsinAndCountryFromLink(url);
  const apiKey = '';
  const apiUrl = 'https://amazon-product-price-data.p.rapidapi.com/product';

  const options = {
    method: 'GET',
    url: apiUrl,
    params: {
      asins: asinValue || asin,
      locale: "IN"
    },
    headers: {
      'X-RapidAPI-Key': apiKey,
      'X-RapidAPI-Host': 'amazon-product-price-data.p.rapidapi.com'
    }
  };

  try {
    const response = await axios.request(options);

    if (response.status === 200) {
      const data = response.data;
      console.log(data)
      return data;
    } else {
      throw new Error('Request failed with status: ');
    }
  } catch (error) {
    console.error('Error fetching data:', error);
    return null;
  }
}

// Example usage
// const asins = 'B005YQZ1KE,B074R8RQQ2'; // Replace with your ASINs
// const locale = 'US'; // Replace with your desired locale


  
  // Example usage
  // const amazonLink = 'https://www.amazon.in/Kingston-PCle-NVtMe-Internal-500GB/dp/B0BBWJH1P8/ref=sr_1_1?keywords=kingston%2Bssd%2B500gb&qid=1692183885&sprefix=kingston%2Caps%2C231&sr=8-1&th=1';
  // const extractedData = extractAsinAndCountryFromLink(amazonLink);
  
module.exports = {
  getProductPriceData
};
