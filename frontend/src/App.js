
import { Route, Routes } from "react-router-dom";
import Home from "./routes/Home";
import About from "./routes/About";
import Contact from "./routes/Contact";
import Service from "./routes/Service";
import './styles.css';
import Registered from "./routes/Register";
import axios from 'axios';
import { UserContextProvider } from "./components/userContext";
import Dash from "./routes/Dashboard";
import Add from "./routes/Addproduct";
import { ToastContainer } from "react-toastify";

axios.defaults.baseURL = 'http://localhost:4000/admin'
axios.defaults.withCredentials = true


export default function App() {
  return (
    <UserContextProvider>

      <div class="App">
      <ToastContainer
      position="bottom-right"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
      theme="dark"/>
        <Routes>
          
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/service" element={<Service />} />
          <Route path="/Register" element={<Registered />} />
          <Route path="/addproduct" element={<Add />} />
        </Routes>
      </div>

      <div >
        <Routes>
          <Route path="/dashboard" element={<Dash />} />
         
        </Routes>
      </div>
    </UserContextProvider>
  );
}