
import React, { useState } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate  } from 'react-router-dom';
import { Link } from 'react-router-dom';
import "./addproduct.css";

const AddProduct = () => {
  const ID = localStorage.getItem('email');

  const [product, setProduct] = useState({
    USERID: ID,
    productName: '',
    CurrentPrice:0,
    RequestedPrice: 0,
  });

  var navigate = useNavigate(); 

  const handleChange = (event) => {
    const { name, value } = event.target;
    setProduct((prevProduct) => ({
      ...prevProduct,
      [name]: value,
    }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    // Send the product data to the backend using Axios POST request
    axios
      .post('http://127.0.0.1:4000/admin/addproducts', product)
      .then((response) => {
        console.log('Product added successfully:', response.data);

        toast.success('Product added successfully!', {
            onClose: () => {
              // Redirect to the product listing component
              navigate('/productlist');
            },
          });

        // You can perform additional actions after successful product addition
      })
      .catch((error) => {
        console.error('Error adding product:', error);
        toast.error('Error adding product. Please try again.');
      });
  };



  return (
   <div className='App-headers'>
    <div>
    <div className="signup_container">
    <div className="signup_form_container">
    <div className="left">
      <h1 class="title">Add Product Details</h1>
      </div>
      <form className ="form_container" onSubmit={handleSubmit}>
        <div className="right">
        <div className="form_container">
      
          <label className="form-label">Product URL:</label>
          <input
            type="text"
            className="input"
            name="productName"
            value={product.productName}
            onChange={handleChange}
            required
          />
{/*         
        <div className='form_container'>
          <label className="form-label">Current Price</label>
          <input
            type="number"
            className="input"
            name="CurrentPrice"
            value={product.CurrentPrice}
            onChange={handleChange}
            required
          />
        </div> */}

        <div className='form_container'>
          <label className="form-label">Discounted Price:</label>
          <input
            type="number"
            className="input"
            name="RequestedPrice"
            value={product.RequestedPrice}
            onChange={handleChange}
            required
          />
          </div>
        </div>
        <button type="submit" className="white_btn" >Add Product</button><br/>
        <button type="submit "className="white_btn" ><Link class="btnn1" to="/dashboard">Dashboard</Link></button><br/>
        </div>
      </form>

    </div>
    </div>
    </div>
    </div>

  );
};

export default AddProduct;
