import { Component } from "react";

import "./dashboard.css";

class DData extends Component {
  render() {
    return (
      <div className={this.props.className}>
        <div className="prod-text">
          <h2>{this.props.heading}</h2>
          <p>{this.props.text}</p>
        </div>
      </div>
    );
  }
}

export default DData;
