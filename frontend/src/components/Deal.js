import "./DealStyles.css";
import Dealdata from "./DealData";
import Deal1 from "../assets/deal1.jpg";
import Deal2 from "../assets/deal2.jpg";
import Deal3 from "../assets/deal3.jpg";

function Deal() {
  return (
    <div className="deal">
      <h1>Current Deals</h1>
      <p>Revamp Your Space with the Hottest Deals of the Season</p>
      <div className="dealcard">
        <Dealdata
          image={Deal1}
          heading="Campus Men's Maxico Running Shoes"
          text="Sole: Phylon
        Closure: Lace-Up
        Fit Type: Regular
        Shoe Width: Medium
        Material Type: Mesh
        Lifestyle: Sports
        Warranty Type: Manufacturer
        Care Instructions: Allow your pair of shoes to air and de-odorize at a regular basis, this also helps them retain their natural shape; use shoe bags to prevent any stains or mildew; dust any dry dirt from the surface using a clean cloth, do not use polish or shiner"
        />

        <Dealdata
          image={Deal2}
          heading="boAt Stone 352 Bluetooth Speaker"
          text="Power- Get ready to be enthralled by the 10W RMS stereo sound on Stone 352 portable wireless speakers.
          IP Rating- With a speaker that offers an IPX7 marked resistance against water and splashes, you can enjoy your playlists across terrains in a carefree way. Charging Time About 1.5-2 hours
          Playback- The speaker offers up to a total of 12 hours of playtime per single charge at 60% volume level. Bluetooth Range - 10m
          True Wireless- It supports TWS functionality.
          "
        />

        <Dealdata
          image={Deal3}
          heading="DHRUVI TRENDZ Men Fancy Shirt"
          text="Care Instructions: Hand Wash Only
          Fit Type: Regular Fit
          Fabric:- Rayon Stylish Shirt || Print:- Tropical Printed Shirt For Boy || Package Contain:- 1 Casual Shirt For Men.
          Sleeves:- Preppy Short Sleeve Stylish Shirt For Men || Neck:- Hawaii Collar shirt for boys || Patten:- Shirt has a full button placket and a curved hem design.
          Occasion:- Casual Wear || Beach Wear || Office Wear|| Formal wear|| Evening wear||
          "
        />
      </div>
    </div>
  );
}

export default Deal;
