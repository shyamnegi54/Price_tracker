import React from "react";
import * as Components from "./Components";
import { useState } from 'react'
import axios from 'axios';
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";




const Register = () => {

  const navigate = useNavigate()
  const [error, setError] = useState();
  const [Data, SetData] = useState(
    {
      Name: '',
      email: '',
      password: ''
    }
  )
  const [data, setData] = useState({ email: "", password: "" });

  const registerUser = async (e) => {
    e.preventDefault()

    try {
      const url = "http://127.0.0.1:4000/admin/register";
      const { Data: res } = await axios.post(url, Data);

      console.log(res.message);

      SetData({})
      toast.success('Register Successful', {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
      })



    } catch (error) {
      if (
        error.response &&
        error.response.status >= 400 &&
        error.response.status <= 500
      ) {
        setError(error.response.data.message);
        toast.error('Registration Failed !', {
          position: "bottom-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "dark",
        });
      }
    }
  }

  const loginUser = async (e) => {
    e.preventDefault();
    try {
      const url = "http://localhost:4000/admin/login";
      const { data: res } = await axios.post(url, data);
      localStorage.setItem("token", res.data);
      localStorage.setItem("email", res.email);
      console.log("EMAIL: ", localStorage.getItem("email"))


      if (data.error) {
        toast.error(data.error)

      } else {
        SetData({})
        navigate("/dashboard")
        toast.success('Login Successful', {
          position: "bottom-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "dark",
        });

      }

    } catch (error) {
      if (
        error.response &&
        error.response.status >= 400 &&
        error.response.status <= 500
      ) {
        setError(error.response.data.message);
      }

    }
  }
  const [signIn, toggle] = React.useState(true);

  return (
    <Components.Container>
      <Components.SignUpContainer signinIn={signIn}>
        <Components.Form onSubmit={registerUser} >
          <Components.Title>Create Account</Components.Title>
          <Components.Input type="text" placeholder="Name" required value={Data.Name} onChange={(e) => SetData({ ...Data, Name: e.target.value })} />
          <Components.Input type="email" placeholder="Email" autocomplete="current-email" required value={Data.email} onChange={(e) => SetData({ ...Data, email: e.target.value })} />
          <Components.Input type="password" placeholder="Password" required value={Data.password} autocomplete="current-password" onChange={(e) => SetData({ ...Data, password: e.target.value })} />
          {error && <div>{error}</div>}
          <Components.Button type='submit'> Sign Up</Components.Button>
        </Components.Form>
      </Components.SignUpContainer>

      <Components.SignInContainer signinIn={signIn}>
        <Components.Form onSubmit={loginUser}>
          <Components.Title>Sign in</Components.Title>
          <Components.Input type="email" placeholder="Email" required value={data.email} autocomplete="current-email" onChange={(e) => setData({ ...data, email: e.currentTarget.value })} />
          <Components.Input type="password" placeholder="Password" autocomplete="current-password" required value={data.password} onChange={(e) => setData({ ...data, password: e.currentTarget.value })} />
          <Components.Anchor href="#">Forgot your password?</Components.Anchor>
          {error && <div>{error}</div>}
          <Components.Button type='submit'>Sign In</Components.Button>
        </Components.Form>
      </Components.SignInContainer>

      <Components.OverlayContainer signinIn={signIn}>
        <Components.Overlay signinIn={signIn}>
          <Components.LeftOverlayPanel signinIn={signIn}>
            <Components.Title>Welcome Back!</Components.Title>
            <Components.Paragraph>
              To keep connected with us please login with your personal info
            </Components.Paragraph>
            <Components.GhostButton onClick={() => toggle(true)}>
              Sign In
            </Components.GhostButton>
          </Components.LeftOverlayPanel>

          <Components.RightOverlayPanel signinIn={signIn}>
            <Components.Title>Hello, Friend!</Components.Title>
            <Components.Paragraph>
              Enter Your personal details and start journey with us
            </Components.Paragraph>
            <Components.GhostButton onClick={() => toggle(false)}>
              Sign Up
            </Components.GhostButton>
          </Components.RightOverlayPanel>
        </Components.Overlay>
      </Components.OverlayContainer>
    </Components.Container>
  );
}
export default Register;
