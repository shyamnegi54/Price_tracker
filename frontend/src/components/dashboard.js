
import { useContext, useEffect, useState } from "react"
import { UserContext } from "./userContext"
import { Link } from "react-router-dom";
import "./dashboard.css";
import "../App.css";
import ProductList from "./listproduct";
import DData from "./dashboarddata";
import axios from "axios";

import Cookies from 'js-cookie';

import { useNavigate } from "react-router-dom";

export default function Dashboard() {
  const { user } = useContext(UserContext)
  const navigate = useNavigate()
  const [userName, setUserName] = useState("")

  const fetchUserData = async () => {
    try {
      const response = await axios.get("/profile")
      const userName = response.data.name;
      setUserName(userName)
    } catch (error) {
      console.error("Error fetching profile data: ", error)
    }
  };
  
  const fetchLogout = async () => {
    try {
     await axios.get("/logout")
    } catch (error) {
      console.error("Error fetching profile data: ", error)
    }
  };

  function removeAllCookies() {
    const cookies = Object.keys(Cookies.get());
    cookies.forEach(cookieName => {
      Cookies.remove(cookieName);
    });
  }

  const handleLogout = () => {
    removeAllCookies()
    fetchLogout()
    localStorage.clear();
    navigate("/register")
  }
  useEffect(() => {
    fetchUserData()
  }, [userName])

  return (
    <div className="bgs">
      <div className="product">
        <div>
          <ProductList />
        </div>
        <h1><span style={{ color: "#003366" }}>DASH</span><span>BOARD</span></h1>
        <p>{
          !!user && (<h3>Welcome Back <span style={{ color: "#003366" }} >{userName}</span></h3>)

        }</p>
        <DData
          className="first-prod"
          text=""
        />
        <p><button className="btnn" >
          <Link className="nav-link" to="/addproduct">
            Add Product</Link >
        </button></p><br />
        <p>
          <button className="btnn " onClick={handleLogout}>
            {/* <Link className="nav-link" to="/register"> */}
            Logout
            {/* </Link > */}
          </button></p>




        {/* <DData
        className="first-prod-reverse"
        heading="Razer DeathAdder Essential"
        text="Always stay in control. The Razer DeathAdder Essential features a true 6,400 DPI optical sensor with a proven history of performance for fast and precise swipes. This gives you smooth and seamless control in the face of a chaotic battle. Maintain high performance during gaming marathons. The ergonomic form provides a comfortable fit for your hands, so you'll never falter in the heat of battle during long hours of gameplay."

      /> */}

      </div>
    </div>
  );
}
