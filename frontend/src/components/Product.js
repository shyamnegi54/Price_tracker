import Product1 from "../assets/prod1.jpg";
import Product2 from "../assets/prod2.jpg";
import Product3 from "../assets/prod3.jpg";
import Product4 from "../assets/prod4.jpg";
import ProductData from "./ProductData";
import "./ProductStyles.css";

const Product = () => {
  return (
    <div className="product">
      <h1>Popular Products</h1>
      <p>Explore our top picks, the best in the market!</p>

      <ProductData
        className="first-prod"
        heading="Fire-Boltt Phoenix Pro"
        text="Comes with a 1.39 TFT Color Full Touch Screen and a 240*240 Pixel
      High Resolution this watch is covered to flaunt the sleek looks with
      a 280 NITS peak brightness The watch will work on a single charge
      for about 7 days (without Bluetooth calling) and about 4 Days with
      Bluetooth calling. Charging Specs - The watch needs to be charged
      for 3 hours to reach 100%. The charger should be a 3.7V to 5V
      adapter or any laptop output. For a bare minimum of 20% charge the
      watch needs to be charged for about 30-40 mins"
        img1={Product1}
        img2={Product2}
      />

      <ProductData
        className="first-prod-reverse"
        heading="Razer DeathAdder Essential"
        text="Always stay in control. The Razer DeathAdder Essential features a true 6,400 DPI optical sensor with a proven history of performance for fast and precise swipes. This gives you smooth and seamless control in the face of a chaotic battle. Maintain high performance during gaming marathons. The ergonomic form provides a comfortable fit for your hands, so you'll never falter in the heat of battle during long hours of gameplay."
        img1={Product3}
        img2={Product4}
      />
    </div>
  );
};

export default Product;
