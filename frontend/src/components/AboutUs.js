import "../components/AboutUsStyles.css";

function AboutUs() {
  return (
    <div className="about-container">
      <h1>Our History</h1>
      <p>
        Established in 2023, Price Tracker has been at the forefront of helping
        shoppers make informed purchasing decisions. Since our inception, we've
        been dedicated to revolutionizing the way people shop by providing
        real-time price tracking and historical data. Our journey began with a
        passion for empowering consumers with the knowledge they need to find
        the best deals and save money. Over the years, we've grown into a
        trusted platform, continuously expanding our reach and capabilities to
        serve our users better.
      </p>

      <h1>Our Mission</h1>
      <p>
        At PriceTrackr, our mission is simple yet powerful: to empower shoppers
        with the tools and insights they need to save money and make confident
        buying choices. We believe that everyone deserves access to accurate,
        up-to-date pricing information, enabling them to make informed decisions
        and maximize their purchasing power. By offering a comprehensive
        platform that tracks prices across a wide range of products, we aim to
        simplify the shopping experience and promote smarter, more economical
        spending habits.
      </p>

      <h1>Our Vision</h1>
      <p>
        Our vision is to become the ultimate resource for consumers seeking
        transparency and affordability in their shopping endeavors. We envision
        a world where individuals can effortlessly track prices, anticipate
        trends, and seize opportunities to save. We aspire to be the go-to
        destination that not only helps users find the best deals but also
        educates them about market dynamics, enabling them to become savvy and
        confident shoppers. As we move forward, we remain committed to enhancing
        our technology and expanding our services to empower even more people on
        their journey to smarter shopping.
      </p>
    </div>
  );
}

export default AboutUs;
