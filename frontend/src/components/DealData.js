import "./DealStyles.css";

function Dealdata(props) {
  return (
    <div className="d-card">
      <div className="d-image">
        <img alt="image" src={props.image} />
      </div>
      <h4>{props.heading}</h4>
      <p>{props.text}</p>
    </div>
  );
}

export default Dealdata;
