import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link} from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate  } from 'react-router-dom';
import "./listproduct.css";

const ProductList = () => {
  const [products, setProducts] = useState([]);
  const navigate = useNavigate(); 
  const emailID = localStorage.getItem("email");

  useEffect(() => {
    // Fetch the list of products from the API
    console.log("EID: ", emailID)
    axios
      .post('http://127.0.0.1:4000/admin/productlist',
      {
          email: emailID,
    })
      .then((response) => {
        setProducts(response.data.data);
        console.log(response.data,"Res Data")
      })
      .catch((error) => {
        console.error('Error fetching products:', error);
      });
  }, []);

  const handleDelete = (productId) => {
    // Delete the product using the API
    axios
      .delete(`http://127.0.0.1:4000/admin/deleteproduct/${productId}`)
      .then(() => {
        // Filter out the deleted product from the state
        setProducts((prevProducts) =>
          prevProducts.filter((product) =>  product._id !== productId)
        );

        // Show toast notification for successful deletion
        toast.success('Product deleted successfully!');
        // history.push('/product-list');
        navigate('/dashboard');
      })
      .catch((error) => {
        console.error('Error deleting product:', error);
        toast.error('Error deleting product. Please try again.');
      });
  };

  return (
    <div className="container">
        
      <div className='body'>
      <table className="table">
        <thead className='thead'>
          <tr className=' tr'>
            <th className='th'>Product Name</th>
            <th className='th' >Current Price</th>
            <th className='th' >Requested Price</th>
            <th className='th'>Actions</th>
          </tr>
        </thead>
        <tbody >
            
          {products.map((product) => (
            <tr className='tr' key={product._id}>
              <td className='td'>{product.productName}</td>
              <td className='td'>{product.CurrentPrice}</td>
              <td className='td'>{product.RequestedPrice}</td>
              <td className='td'>
                <Link to={`/update/${product._id}`}>
                  <FontAwesomeIcon icon={faEdit} className="me-2" />
                </Link>
                <FontAwesomeIcon
                  icon={faTrash}
                  className="text-danger"
                  style={{ cursor: 'pointer' }}
                  onClick={() => handleDelete(product._id)}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      </div>
    </div>
  );
};

export default ProductList;











