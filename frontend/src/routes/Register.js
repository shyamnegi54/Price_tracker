
import Navbar from "../components/Navbar";

import Footer from "../components/Footer";
import Register from "../components/registration";
import "../App.css";


function Registered() {
  return (
    <>
    
      <Navbar />
      <div class="Register" >
      <Register />
      </div>

      <Footer />
    </>
  );
}

export default Registered;
