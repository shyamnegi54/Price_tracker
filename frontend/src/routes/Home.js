import Hero from "../components/Hero";
import Navbar from "../components/Navbar";
import AboutImg from "../assets/first.jpg";
import Product from "../components/Product";
import Deal from "../components/Deal";
import Footer from "../components/Footer";
import "../styles.css"
import { Link } from "react-router-dom";
function Home() {
  return (
    <div className=" App">
      <Navbar />
      <Hero
        cName="hero"
        heroImg={AboutImg}
        title="Real-Time Price Monitoring"
        text="Never Miss a Sale! Check Out Now!"
        buttonText="Tracker Plan"
        url="/register"
        btnClass="show"
      />
      <Product />
      <Deal />
      <Footer />
    </div>
  );
}

export default Home;
