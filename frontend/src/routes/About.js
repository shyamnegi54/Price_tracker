import Hero from "../components/Hero";
import Navbar from "../components/Navbar";
import AboutImg from "../assets/AboutImg.jpg";
import Footer from "../components/Footer";
import AboutUs from "../components/AboutUs";
import "../styles.css"
function About() {
  return (
    <div className="App" >
      <Navbar />
      <Hero cName="hero-mid" heroImg={AboutImg} btnClass="hide" />
      <AboutUs />
      <Footer />
    </div>
  );
}

export default About;
