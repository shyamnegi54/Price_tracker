import Hero from "../components/Hero";
import Navbar from "../components/Navbar";
import Services from'../assets/ServiceImg.jpg';
import Footer from "../components/Footer";
import Deal from "../components/Deal";

function Service() {
  return (
    <>
      <Navbar />
      <Hero cName="hero-mid" heroImg={Services} btnClass="hide" />
      <Deal />
      <Footer />
    </>
  );
}

export default Service;
